# ACE3P Extensions

ACE3P (Advanced Computational Electromagnetics 3D Parallel) is a comprehensive
set of conformal, higher-order, parallel finite-element electromagnetic codes
with multi-physics capabilities in integrated electromagnetic, thermal and
mechanical simulation. ACE3P is based on higher-order curved finite elements
for high­‐fidelity modeling and improved solution accuracy. It is implemented
on massively parallel computers for increased memory (problem size) and
speed. More information on ACE3P is at
https://confluence.slac.stanford.edu/display/AdvComp/ACE3P+-+Advanced+Computational+Electromagnetic+Simulation+Suite


This project provides extensions for
[SMTK](https://www.computationalmodelbuilder.org/smtk/) to import, export and manipulate data related to ACE3P simulation.
