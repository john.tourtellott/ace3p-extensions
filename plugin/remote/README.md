# Remote Server Configuration Files

The two files starting with "modelbuilder-" are used to configure
modelbuiler for connecting to ParaView servers on NERSC/Cori.

* `modelbuilder-cori-unix.pvsc`
* `modelbuilder-cori-win.pvsc`

Note: the two files starting with "cori" (e.g., `cori-*.pvsc`) are copied
verbatum from the [NERSC ParaView](https://docs.nersc.gov/applications/paraview/)
web page and are included for reference only.
