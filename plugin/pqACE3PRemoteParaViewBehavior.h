//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PRemoteParaViewBehavior_h
#define pqACE3PRemoteParaViewBehavior_h

#include <QObject>

#include <QMap>
#include <QPointer>
#include <QString>

class pqServer;
class QAction;
class QMenu;

/** \brief Adds menu for starting PVServer on remote machines
 *
 *  Hard-coded for NERSC machines: Cori now, Perlmutter later
 */
class pqACE3PRemoteParaViewMenu : public QObject
{
  Q_OBJECT

public:
  /**
  * Assigns the menu that will display the list of remote machines
  */
  pqACE3PRemoteParaViewMenu(QMenu* menu, QObject* parent = nullptr);
  ~pqACE3PRemoteParaViewMenu() override = default;

private slots:
  void buildMenu();
  void onTriggered(QAction*);

private:
  Q_DISABLE_COPY(pqACE3PRemoteParaViewMenu);

  QPointer<QMenu> m_menu;
};

/** \brief Add behavior for starting remote PVServer
  */
class pqACE3PRemoteParaViewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PRemoteParaViewBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PRemoteParaViewBehavior() override;

  pqServer* remoteServer(const QString& machine) const;
  bool startRemoteServer(const QString& machine);

signals:
  void serverAdded(pqServer* server);

protected:
  pqACE3PRemoteParaViewBehavior(QObject* parent = nullptr);

  // Store <machine, pqServer> dictionary
  QMap<QString, pqServer*> m_serverMap;

private slots:
  void serverTimeoutIn5Mins();
  void serverTimeoutIn1Min();
  void serverTimeout();

private:
  Q_DISABLE_COPY(pqACE3PRemoteParaViewBehavior);
};

#endif // pqACE3PRemoteParaViewBehavior_h
