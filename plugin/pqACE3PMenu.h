//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PMenu_h
#define pqACE3PMenu_h

#include "smtk/PublicPointerDefs.h"

#include <QActionGroup>

class QAction;

class pqACE3PRecentProjectsMenu;
class pqACE3PRemoteParaViewMenu;

/** \brief Adds the "ACE3P" menu to the application main window
  */
class pqACE3PMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

public slots:
  void onProjectOpened(smtk::project::ProjectPtr project);
  void onProjectClosed();

public:
  pqACE3PMenu(QObject* parent = nullptr);
  ~pqACE3PMenu() override;

  bool startup();
  void shutdown();

private:
  QAction* m_nerscBrowserAction = nullptr;
  QAction* m_nerscLoginAction = nullptr;
  QAction* m_newProjectAction = nullptr;
  QAction* m_newStageAction = nullptr;
  QAction* m_exportProjectAction = nullptr;
  QAction* m_saveProjectAction = nullptr;
  QAction* m_openProjectAction = nullptr;
  QAction* m_closeProjectAction = nullptr;

  QAction* m_recentProjectsAction = nullptr;
  QAction* m_remoteParaViewAction = nullptr;

  pqACE3PRecentProjectsMenu* m_recentProjectsMenu = nullptr;
  pqACE3PRemoteParaViewMenu* m_remoteParaViewMenu = nullptr;
  bool m_saveMenuConfigured = false;

  Q_DISABLE_COPY(pqACE3PMenu);
};

#endif // pqACE3PMenu_h
