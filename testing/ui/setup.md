# Setup for UI testing

## Purpose

Verify the availability and basic functionality of modelbuilder release packages.

## Prerequisites

In standard practice, the UI test procedures should be initiated by an issue created in the ace3p-extensions project, https://gitlab.kitware.com/cmb/plugins/ace3p-extensions/-/issues.

* The title of the GitLab issue should be of the form `UI testing for modelbuilder yyyy-mm-dd`.
* The description of the GitLab issue should include the download location. The standard location for modelbuilder downloads is https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/5980ed698d777f16d01ea0e8, but alternative builds can be used as long as they are documented in the GitLab issue.
* The platform(s) to be tested should also be included in the description of GitLab issue.

## Procedure

This procedure should be run once before any other test procedures.

[ ] Add a comment to the GitLab issue indicating which packages are available, including those not used for testing. You can abbreviate using "linux", "macOS", and "windows".

[ ] On the test machine, move any previous modelbuilder downloads/installs to the trash folder.

[ ] Create a test directory using a naming convention such as `.../cmb/testing/yyyy-mm-dd`.

[ ] Download the modelbuilder package to test and unzip into the test directory. Note that it might take several minutes to unzip the Windows package.

[ ] Delete any previous modelbuilder settings files:

* On linux, the directory is `~/.config/modelbuilder/`.
* On macOS, the directory is `~/.config/modelbuilder/`.
* On windows, the directory is `~/AppData/Roaming/modelbuilder/`.

[ ] Run the modelbuilder executable.

[ ] Click on the toolbar button "Apply changes to parameters automatically". The icon is in the first (top) toolbar and looks like a 3D box with an arrow pointing to the upper right.

[ ] Close all panels except for "Attribute Editor", "Output Messages", "Resources". Undock and drag the "Attribute Editor" over the "Resources" panel so that they dock as tabs.

[ ] In the View => Toolbars menu item, uncheck the "Measurement Tools" item to hide the "Ruler" icon.

[ ] Load the project manager plugin:

* Select Tools => Manage Plugins to open the Plugin Manager dialog.
* Select the "smtkProjectManagerPlugin" in the list of Local Plugins.
* Click "Load Selected".
* Open the smtkProjectManagerPlugin options and check the "Auto Load" checkbox.
* Close the Plugin Manager dialog.
* Verify that modelbuilder menubar now includes a "Project" menu.

[ ] Exit modelbuilder.

[ ] Restart modelbuilder and verify that the widget layout is the same and that the "Project" menu is displayed.
