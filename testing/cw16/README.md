# CW16 Tests

The tests in this folder attempt to duplicate examples that are included in the SLAC 2016 Code Workshop. The tests do minimal validation, but write resource and ACE3P input files to folders organized under `Testing/Temporary/cw16` so that use cases can be reliably reproduced. Each test uses a .yml file to specific attributes to be created and edited.

Note: the 2016 Code Workshop data are used in lieu of the 2018 workshop, because the 2018 workshop data did not include .ncdf or .get files with most examples.
