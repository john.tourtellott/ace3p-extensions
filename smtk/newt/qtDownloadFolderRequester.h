
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_newt_qtDownloadFolderRequester_h
#define smtk_newt_qtDownloadFolderRequester_h

#include "smtk/newt/Exports.h"

#include <QObject>
#include <QString>

class QObject;

/* \brief Utility class for downloading folders using qtNewtInterface.
  *
  */

namespace newt
{

class SMTKNEWT_EXPORT qtDownloadFolderRequester : public QObject
{
  Q_OBJECT

public:
  qtDownloadFolderRequester(QObject* parent = nullptr);
  ~qtDownloadFolderRequester();

  void startDownload(
    const QString& localFolder,
    const QString& machine,
    const QString& remoteFolder,
    bool recursive = true,
    const QString& fileFilter = "");

  bool isBusy() const;

signals:
  void errorMessage(const QString& text);
  void progressMessage(const QString& text);
  void downloadComplete();

protected:
  // Internal method which can be called recursively
  void startInternal(
    const QString& localFolder,
    const QString& machine,
    const QString& remoteFolder,
    bool recursive,
    const QString& regexPattern);

  void downloadFile(const QString& localPath, const QString& remotePath);

  void checkIfComplete();

protected slots:
  void onListFolderReply();

private:
  class Internal;
  Internal* m_internal;
};

} // namespace newt
#endif
