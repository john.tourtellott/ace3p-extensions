//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtStagesWidget
#define smtk_simulation_ace3p_qt_qtStagesWidget

#include "smtk/PublicPointerDefs.h"
#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QString>
#include <QWidget>

class QItemSelection;
class qtStagesModel;

namespace newt
{
class qtDownloadFolderRequester;
}

namespace Ui
{
class qtStagesWidget;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtStagesModel;

class SMTKACE3PQTEXT_EXPORT qtStagesWidget : public QWidget
{
  Q_OBJECT

public:
  qtStagesWidget(QWidget* parentWidget = nullptr);
  ~qtStagesWidget() = default;

  /** \brief Sets visible GUI highlighting for the current selection. */
  void setGUIRowSelection(int index);

signals:
  /** \brief Signal for "Add..." button click. */
  void addStageClicked();
  /** \brief Signal for "Delete" button click. */
  void deleteStageClicked(int index);
  /** \brief Signal for "Select" button click. */
  void selectStageClicked(int index);

public slots:
  /** \brief Calls addStage() for the table model. */
  void onStageAdded(int stageNumber);
  /** \brief Passes the currently selected Stage down to the table model. */
  void onUpdateProjectCurrentStage(int currentStage);
  /** \brief Calls setProject() for the table model. */
  void setProject(smtk::project::ProjectPtr project);
  /** \brief Calls setProject() for the table model (with `nullptr`). */
  void unsetProject();

protected slots:
  void on_pushButton_Add_clicked();
  void on_pushButton_Delete_clicked();
  void on_m_stagesTable_clicked(const QModelIndex& index);

protected:
  // Qt model for the analysis table
  qtStagesModel* m_stages_model;

private:
  // @brief pointer to UI information
  Ui::qtStagesWidget* ui;

  int getViewSelectedRowIndex();
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
