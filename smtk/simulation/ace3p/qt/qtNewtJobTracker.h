
//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtNewtJobTracker_h
#define smtk_simulation_ace3p_qt_qtNewtJobTracker_h

#include <QObject>
#include <QString>

#include "smtk/simulation/ace3p/qt/Exports.h"

#include "smtk/newt/qtNewtInterface.h"
#include "smtk/simulation/ace3p/qt/qtAbstractJobTracker.h"

class QNetworkReply;

namespace smtk
{
namespace simulation
{
namespace ace3p
{

/* \brief Manages Newt I/O for tracking job progress.
 *
 * Accepts jobs to track and makes Newt requests to get status
 * at periodic intervals. Emits signal for each status update.
 * Stops tracking job when its status is terminal (complete or error).
 * Also supports single-shot update
 *
 */
class SMTKACE3PQTEXT_EXPORT qtNewtJobTracker : public qtAbstractJobTracker
{
  Q_OBJECT
  using Superclass = qtAbstractJobTracker;

public:
  qtNewtJobTracker(QObject* parent = nullptr);
  ~qtNewtJobTracker() override = default;

  // Gets status for each job.
  bool pollOnce() override;

protected slots:
  void onNewtReply();
  void onLogin();

protected:
  // Make job request to Newt
  void requestJob(int index) override;

  // Checks if timer should be started for next polling interval.
  // The pollNow option will do the first poll immediately.
  bool setNextPoll(bool pollNow = false) override;

private:
  newt::qtNewtInterface* m_newt = nullptr;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk
#endif
