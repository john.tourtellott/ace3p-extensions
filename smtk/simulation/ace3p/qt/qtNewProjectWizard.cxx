//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtNewProjectWizard.h"

#include "smtk/simulation/ace3p/qt/qtNewProjectMeshPage.h"
#include "smtk/simulation/ace3p/qt/qtNewProjectNamePage.h"
#include "smtk/simulation/ace3p/qt/qtNewProjectOperatePage.h"

#include <QAbstractButton>
#include <QCoreApplication>
#include <QLabel>
#include <QString>
#include <QVBoxLayout>
#include <QWidget>
#include <QWizardPage>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtNewProjectWizard::qtNewProjectWizard(QWidget* parent)
  : Superclass(parent)
  , m_namePage(nullptr)
  , m_meshPage(nullptr)
  , m_operatePage(nullptr)
{
  this->setWindowTitle("Create ACE3P Project");
  this->setOptions(
    QWizard::NoDefaultButton | QWizard::NoBackButtonOnStartPage | QWizard::NoBackButtonOnLastPage |
    QWizard::NoCancelButtonOnLastPage);

  // Use modern style to avoid macos sidebar
  this->setWizardStyle(QWizard::ModernStyle);

  // Construct pages
  m_namePage = new qtNewProjectNamePage();
  this->addPage(m_namePage);

  m_meshPage = new qtNewProjectMeshPage();
  this->addPage(m_meshPage);

  m_operatePage = new qtNewProjectOperatePage();
  this->addPage(m_operatePage);

  auto* finalPage = new QWizardPage();
  finalPage->setTitle("Finished");
  QString labelText = "The new project has been created. "
                      "Go to the Attribute Editor to specify the analysis.";
  auto* label = new QLabel(labelText, finalPage);
  label->setWordWrap(true);
  finalPage->setButtonText(QWizard::FinishButton, "Close");
  finalPage->setCommitPage(true);
  finalPage->setFinalPage(true);

  auto* layout = new QVBoxLayout();
  layout->addWidget(label);
  finalPage->setLayout(layout);
  this->addPage(finalPage);

  // Setup connections
  QObject::connect(this, &QWizard::currentIdChanged, this, &qtNewProjectWizard::pageChanged);
}

void qtNewProjectWizard::setProjectManager(std::shared_ptr<smtk::project::Manager> projectManager)
{
  m_operatePage->setProjectManager(projectManager);
}

QString qtNewProjectWizard::projectLocation() const
{
  return m_namePage->projectLocation();
}

void qtNewProjectWizard::setProjectLocation(const QString& path)
{
  m_namePage->setProjectLocation(path);
}

QString qtNewProjectWizard::meshFileDirectory() const
{
  return m_meshPage->meshFileDirectory();
}

void qtNewProjectWizard::setMeshFileDirectory(const QString& path)
{
  m_meshPage->setMeshFileDirectory(path);
}

std::shared_ptr<Project> qtNewProjectWizard::project() const
{
  if (m_operatePage != nullptr)
  {
    return m_operatePage->project();
  }
  // (else)
  return nullptr;
}

void qtNewProjectWizard::pageChanged(int id)
{
  QWizardPage* page = this->page(id);

  // On name page, call method to check the name
  auto* namePage = dynamic_cast<qtNewProjectNamePage*>(page);
  if (namePage != nullptr)
  {
    namePage->selectName();
    return;
  }

  // On operate page, call method to run the create operation
  auto* operatePage = dynamic_cast<qtNewProjectOperatePage*>(page);
  if (operatePage != nullptr)
  {
    this->button(QWizard::BackButton)->hide();
    this->button(QWizard::CancelButton)->hide();
    this->button(QWizard::NextButton)->setEnabled(false);

    // Make sure UI updates to display the operation page
    QCoreApplication::processEvents();

    operatePage->operate();
  }
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
