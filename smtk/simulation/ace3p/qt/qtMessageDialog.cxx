//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "qtMessageDialog.h"

#include <QAbstractButton>
#include <QApplication>
#include <QBuffer>
#include <QCommonStyle>
#include <QCoreApplication>
#include <QMutex>
#include <QProgressDialog>
#include <QTimer>

// minimal constructor
qtMessageDialog::qtMessageDialog(QWidget* parent)
  : QMessageBox(parent)
{
  initialize();
}

// constructor
qtMessageDialog::qtMessageDialog(
  Icon icon,
  const QString& title,
  const QString& text,
  StandardButtons buttons,
  QWidget* parent,
  Qt::WindowFlags flag)
  : QMessageBox(icon, title, text, buttons, parent, flag)
{
  initialize();
}

// common initialization
void qtMessageDialog::initialize()
{
  setWindowTitle("Information");
  m_autocloseTimer = new QTimer(this);
  QObject::connect(
    m_autocloseTimer, &QTimer::timeout, this, &qtMessageDialog::processAutoCloseTimer);

  m_countdownButton = QMessageBox::Ok;
  m_bAutoClose = false;
  m_autoCloseDelay = 0;
}

// destructor
qtMessageDialog::~qtMessageDialog()
{
  ;
}

void qtMessageDialog::centerOnParent()
{
  if (!parentWidget())
  {
    return;
  }

  int width = this->width();
  int height = this->height();

  int x = parentWidget()->pos().x() + parentWidget()->width() / 2 - width / 2;
  int y = parentWidget()->pos().y() + parentWidget()->height() / 2 - height / 2;
  this->setGeometry(x, y, width, height);
}

int qtMessageDialog::exec()
{
  centerOnParent();
  startAutocloseTimer();

  return QMessageBox::exec();
}

void qtMessageDialog::show()
{
  centerOnParent();
  startAutocloseTimer();

  QMessageBox::show();
}

void qtMessageDialog::startAutocloseTimer()
{
  if (m_bAutoClose)
  {
    if (m_autoCloseDelay == 0)
    {
      this->close();
      return;
    }
    if (this->buttons().size() == 0)
    {
      this->setStandardButtons(m_countdownButton);
    }

    m_remainingAutoCloseSeconds = m_autoCloseDelay;
    QAbstractButton* button = this->button(m_countdownButton);
    m_originalButtonText = button->text();
    button->setText(QString("%1 (%2)").arg(m_originalButtonText).arg(m_remainingAutoCloseSeconds));
    m_autocloseTimer->start(1000);
  }
}

// sets the dialog to automatically close upon progress bar completion
void qtMessageDialog::setAutoClose(bool bAutoClose)
{
  m_bAutoClose = bAutoClose;
}

// imposes a delay after the completion of a run, before auto-close is triggered
void qtMessageDialog::setAutoCloseDelay(uint delay)
{
  m_autoCloseDelay = delay;
}

// process auto-close timer ticks
void qtMessageDialog::processAutoCloseTimer()
{
  m_remainingAutoCloseSeconds -= 1;
  if (m_remainingAutoCloseSeconds <= 0)
  {
    m_autocloseTimer->stop();
    this->close();
  }
  else
  {
    QAbstractButton* button = this->button(m_countdownButton);
    button->setText(QString("%1 (%2)").arg(m_originalButtonText).arg(m_remainingAutoCloseSeconds));
  }
}
