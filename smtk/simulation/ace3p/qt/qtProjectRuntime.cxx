//=============================================================================
//
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//
//=============================================================================
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

namespace
{
// Static var to store the singleton instance
static smtk::simulation::ace3p::qtProjectRuntime* g_instance = nullptr;
} // namespace

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtProjectRuntime* qtProjectRuntime::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new qtProjectRuntime(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

qtProjectRuntime::~qtProjectRuntime()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}

bool qtProjectRuntime::unsetProject(std::shared_ptr<smtk::project::Project> p)
{
  if (m_project == p)
  {
    m_project.reset();
    return true;
  }

  // (else)
  return false;
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
