//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtStagesWidget.h"
#include "smtk/simulation/ace3p/qt/qtStagesModel.h"
#include "smtk/simulation/ace3p/qt/ui_qtStagesWidget.h"

#include "smtk/simulation/ace3p/Project.h"
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"

// delete this later - TODO (JOHN)
#include "smtk/simulation/ace3p/testing/cxx/randomJobCreator.h"

#include <QCoreApplication>
#include <QDebug>
#include <QItemSelectionModel>
#include <QMessageBox>
#include <QVariant>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtStagesWidget::qtStagesWidget(QWidget* parentWidget)
  : QWidget(parentWidget)
  , m_stages_model(new qtStagesModel(this))
  , ui(new Ui::qtStagesWidget)
{
  // initialize the UI
  this->ui->setupUi(this);

  // setup stages table
  this->ui->m_stagesTable->setModel(m_stages_model);
  this->ui->m_stagesTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  this->ui->m_stagesTable->setSelectionMode(QAbstractItemView::SingleSelection);
  this->ui->m_stagesTable->setEditTriggers(QAbstractItemView::DoubleClicked);
  this->ui->m_stagesTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  this->ui->m_stagesTable->show();

  // windows only stylesheet declaration to get the horizontal header cells to properly draw their
  //    border lines
  if (QSysInfo::windowsVersion() == QSysInfo::WV_WINDOWS10)
  {
    this->ui->m_stagesTable->horizontalHeader()->setStyleSheet("QHeaderView::section{"
                                                               "border-top:0px solid #D8D8D8;"
                                                               "border-left:0px solid #D8D8D8;"
                                                               "border-right:1px solid #D8D8D8;"
                                                               "border-bottom: 1px solid #D8D8D8;"
                                                               "background-color:white;"
                                                               "}"
                                                               /*
      "QTableCornerButton::section{"
        "border-top:0px solid #D8D8D8;"
        "border-left:0px solid #D8D8D8;"
        "border-right:1px solid #D8D8D8;"
        "border-bottom: 1px solid #D8D8D8;"
        "background-color:white;"
      "}"
      */
    );
  }

  QObject::connect(
    this, &qtStagesWidget::deleteStageClicked, m_stages_model, &qtStagesModel::removeStage);
}

void qtStagesWidget::onStageAdded(int stageNumber)
{
  m_stages_model->addStage(stageNumber);
  QCoreApplication::processEvents(); // needed?

  this->ui->m_stagesTable->selectRow(stageNumber);
  qDebug() << __FILE__ << __LINE__ << "Added & selected stage" << stageNumber;
}

void qtStagesWidget::onUpdateProjectCurrentStage(int currentStage)
{
  qDebug() << __FILE__ << __LINE__ << "onUpdateProjectCurrentStage";
  this->ui->m_stagesTable->selectRow(currentStage);
  emit this->selectStageClicked(currentStage);
}

void qtStagesWidget::setProject(smtk::project::ProjectPtr project)
{
  m_stages_model->setProject(project);
}

void qtStagesWidget::unsetProject()
{
  m_stages_model->setProject(nullptr);
}

void qtStagesWidget::setGUIRowSelection(int index)
{
  this->ui->m_stagesTable->selectRow(index);
}

void qtStagesWidget::on_pushButton_Add_clicked()
{
  smtk::simulation::ace3p::qtProjectRuntime* project = qtProjectRuntime::instance();
  if (!project->ace3pProject())
  {
    QMessageBox::warning(this, "Add Warning", "You must first open/create a project.");
    return;
  }

  emit addStageClicked();
}

int qtStagesWidget::getViewSelectedRowIndex()
{
  QItemSelectionModel* pSelection = this->ui->m_stagesTable->selectionModel();
  QModelIndexList rowList = pSelection->selectedRows();
  if (rowList.isEmpty())
  {
    return -1;
  }

  // 'm_stagesTable->setSelectionMode(QAbstractItemView::SingleSelection)' ensures only one result
  return rowList[0].row();
}

void qtStagesWidget::on_pushButton_Delete_clicked()
{
  qDebug() << "qtStagesWidget::on_pushButton_Delete_clicked()";

  int selectionIndex = this->getViewSelectedRowIndex();
  if (selectionIndex == -1)
  {
    QMessageBox::warning(this, "Selection Warning", "You must first select a stage from the list.");
    return;
  }

  smtk::simulation::ace3p::qtProjectRuntime* project = qtProjectRuntime::instance();
  if (project->ace3pProject()->numberOfStages() < 2)
  {
    QMessageBox::warning(
      this, "Remove Warning", "You cannot remove the last stage from a project.");
    return;
  }

  QMessageBox confirmationCheck(this);
  confirmationCheck.setWindowTitle("Delete Stage?");
  confirmationCheck.setText("Are you sure you would like to delete this Stage?");
  /*QPushButton * yesButton = */ confirmationCheck.addButton(QMessageBox::Yes);
  QPushButton* noButton = confirmationCheck.addButton(QMessageBox::No);
  confirmationCheck
    .exec(); // this is a blocking function call (as opposed to show(), which doesn't block)
  if (confirmationCheck.clickedButton() == noButton)
  {
    return;
  }

  emit deleteStageClicked(selectionIndex);
}

void qtStagesWidget::on_m_stagesTable_clicked(const QModelIndex& index)
{
  if (index.row() != m_stages_model->projectCurrentStage())
  {
    emit this->selectStageClicked(index.row());
  }
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
