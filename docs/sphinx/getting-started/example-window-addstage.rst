Create Stage 2 (Track3P)
========================

To add a second stage to your project, open the :guilabel:`ACE3P` menu and find and click the ``Add Stage...`` item.

.. image:: images/window-add-stage.png
    :align: center

|

In response, modelbuilder will display a dialog where you specify the name and mesh for the new Stage.

.. image:: images/window-new-stage.png
    :align: center

|

For :guilabel:`Stage Name` you can use the default ("Stage 2"). For the geometry, we are going to use the same mesh ("Window") as the first stage. To do this, choose ``Select From Project`` for the :guilabel:`Mesh` parameter, and then select ``Window`` (the only option) for the :guilabel:`Select Mesh` field. Once you click the :guilabel:`Apply` button, a new stage will be added to your project, and the :guilabel:`Analysis Stages` panel will be displayed showing two rows.

.. image:: images/window-stages-panel.png
    :align: center

|

You can switch between stages by selecting either row in the :guilabel:`Analysis Stages` table. Clicking on a stage in the panel will update both the :guilabel:`Attribute Editor` and the 3-D :guilabel:`RenderView`. (Because we are using the same mesh for both stages in this project, the 3-D view does not actually change.)

With the second stage added to the project, we can proceed to specify the :program:`Track3P` simulation.
