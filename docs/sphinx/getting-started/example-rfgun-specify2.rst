Specify TEM3P Analysis and Run
==============================

Now, you need to specify the Analysis Parameters for this second Stage in the Attribute Editor.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Modules Tab

Set the ``Analysis`` parameter to ``TEM3P``.  For the ``TEM3P`` type, select ``Thermal/Elastic``, and then chock both checkboxes.  Next to the ``Thermal`` checkbox, select ``Linear`` from the drop-down menu.

Check the ``Source`` checkbox, and set the path to the ``omega3p_results`` directory found in the same manner as in the :ref:`Window tutorial<Specify Track3P Analysis (part 1)>` (in the Modules Tab section).

.. image:: images/rfgun-modules2.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

2\. TEM3P Elastic - Boundary Conditions Tab

Set the boundary conditions as shown below:

.. image:: images/rfgun-boundary-conditions2a.png
    :align: center

For the ``Mixed`` settings use these settings for (x,y,z):

Component 1: Neumann, Neumann, Dirichlet

Component 2: Dirichlet, Neumann, Neumann

Component 3: Neumann, Dirichlet, Neumann

For Component 6, you will need to set your parameters to match those shown in the image above.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

3\. TEM3P Elastic - Materials Tab

Add a materials by pressing the ``Add`` button, set the Material to Copper in the drop-down menu, and assign ``Unnamed Block ID: 1`` to the Current list.

.. image:: images/rfgun-materials2a.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. TEM3P Elastic - Analysis Tab

Set the Analysis tab parameters as seen in the screenshot below.

.. image:: images/rfgun-analysis2a.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

5\. TEM3P Elastic - Mesh Output Tab

Set the Mesh Output tab parameters as seen in the screenshot below.

.. image:: images/rfgun-mesh-output2a.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

6\. TEM3P Thermal - Boundary Conditions Tab

Set the boundary conditions as shown below:

.. image:: images/rfgun-boundary-conditions2b.png
    :align: center

For Component 5, use the setting below:

Set ``Robin Constant Factor`` to ``20000``

Set ``Robin Constant Value`` to ``440000``

For Component 6, you will need to set your parameters to match those shown in the image above.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

7\. TEM3P Thermal - Materials Tab

Set the parameters as shown in the image below:

.. image:: images/rfgun-materials2b.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

8\. TEM3P Thermal - Analysis Tab

Set the parameters as shown in the image below:

.. image:: images/rfgun-analysis2b.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

9\. Submit Job and Download

Once configured, log in to NERSC and submit the job for simulation.  Once you download the results, they should look like the image below.

.. note::
    Given the size of this simulation, you will want to change the default ``1`` core to ``32`` cores (on ``1`` node)

.. image:: images/rfgun-result2.png
    :align: center

|
