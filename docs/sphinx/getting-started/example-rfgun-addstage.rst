Create Stage 2 (RfGun)
======================

Now, you should add a second stage to your project. In the pop-up screen, Select ``Import New Mesh File`` and browse to find ``RfGunBody.gen``.  You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632>`_ if needed.

For more detail on the specifics of adding a new Stage to your project, refer to the :ref:`Window tutorial<Create Stage 2 (Track3P)>`.

.. image:: images/rfgun-add-new-stage.png
    :align: center

|
