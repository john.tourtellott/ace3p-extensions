Initialize RF-Postprocessing Settings & Submit Analysis
=======================================================

.. rst-class:: step-number

1\. Select Omega3P Job to Postprocess

To begin setting up an :program:`rf-postprocessing` analysis, we need to reference a previously run :program:`Omega3P` simulation job. To do this, go to the :guilabel:`Simulation Job` panel where previously submitted simulation jobs are listed. Click on the :program:`Omega3P` job in the list which you would like to perform postprocessing on. A details panel will open at the bottom of the panel below the jobs list. In the details panel, press the clipboard button next to the :guilabel:`Remote Job Directory` line toward the bottom of the detail panel. This will copy the remote path of the :program:`Omega3P` job to the clipboard.

.. image:: images/rfPost-jobPanelCopyHighlight.png
    :align: center
    :width: 75 %

|

.. rst-class:: step-number

2\. Initialize RF-Postprocessing Settings

Next, navigate to the :guilabel:`Attribute Editor` panel. Under the :guilabel:`Modules` tab, select :guilabel:`ACDTool` from the dropdown list. A second dropdown list will appear where you may select :guilabel:`RF Postprocess`. With that selection made in the dropdown menus, a field will appear labeled :guilabel:`NERSC Data Directory`. Paste the previously copied directory path here with :kbd:`Command-v` on macOS or :kbd:`Control-v` on Windows or by right clicking and selecting :guilabel:`Paste`.

.. image:: images/rfPost-ModulesTab.png
    :align: center
    :width: 75 %

With a reference to the target :program:`Omega3P` job entered, we can move on to the :guilabel:`RF-Postprocess` tab. Under this tab is a menu of many parameters and options for specifying a :program:`rf-postprocessing` analysis.

At the top of the scrollable menu is a box of options labeled :guilabel:`RFField`. In this section, you may edit the :guilabel:`ResultDir` string which indicates where simulation results data were stored within the target simulation folder.

.. note::
    The default entry to the :guilabel:`ResultsDir` field is ``omega3p_results``, matching the default results directory for :program:`Omega3P` simulations. Ensure this string matches what the results directory of your target simulation is called.

.. image:: images/rfPost-rfFieldMenu.png
    :align: center
    :width: 75 %

For this example, within the :guilabel:`RFField` section, we will set

* :guilabel:`xsymmetry`: magnetic
* :guilabel:`ysymmetry`: magnetic
* :guilabel:`gradient`: -1
* :guilabel:`gz1`: -0.057
* :guilabel:`gz2`: 0.0057
* :guilabel:`npoint`: 300
* :guilabel:`fmnx`: 10
* :guilabel:`fmny`: 10
* :guilabel:`fmnz`: 50

Below the :guilabel:`RFField` section is a collection of optional analyses available within an :program:`rf-postprocessing` analysis. Clicking on the checkbox next to any of the options reveals collections of options for specifying those optional analyses. A detailed description of each of the various sub-analyses is available `here <https://www.slac.stanford.edu/%7Echo/cw18/commands/acdtool-commands.pdf>`_.

Under the optional analyses, we will turn on :guilabel:`RoverQ` and :guilabel:`maxFieldsOnSurface` and keep their default values.


.. image:: images/rfPost-OptionalAnalyses.png
    :align: center
    :width: 75 %

|

.. rst-class:: step-number

3\. Submit Postprocessing Job

Submitting an ACDTool :program:`rf-postprocessing` job utilizes the same job export and submission infrastructure as all other remote jobs in ModelBuilder for :program:`ACE3P`. You can review this process :ref:`here<Running Omega3P on NERSC>`. With the job submitted, you can monitor its progress :ref:`from the Simulation Jobs panel<Tracking Job Status>`.
