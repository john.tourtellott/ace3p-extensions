Specify Omega3P Analysis and Run
================================

There are several parameters that need to be set for your first stage.  All these steps will be performed in the Attribute Editor panel.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Modules Tab

Set the ``Analysis`` parameter to ``Omega3P``.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

2\. Boundary Conditions Tab

Set the Boundary Conditions as shown in the image below.

.. image:: images/rfgun-boundary-conditions.png
    :align: center

|

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

3\. Analysis Tab

Change the ``Frequency Shift (Hz)`` parameter to ``2.8e9``.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

4\. Submit Job and Download

Once configured, log in to NERSC and submit the job for simulation.  Once you download the results, they should look like the image below.

.. image:: images/rfgun-result1.png
    :align: center

|
