Closing the Project
===================

.. rst-class:: step-number

16\. Close the Project

When you are done exploring the results, you can open the :guilabel:`ACE3P` menu and select the :guilabel:`Close Project` item to remove it from memory and clear the various view panels.
