Specify Track3P Analysis (part 1)
=================================

Next, we will specify the analysis parameters for the second (:program:Track3P) stage in the :guilabel:`Attribute Editor`.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Select the Track3P Stage

In the :guilabel:`Analysis Stages` panel, make sure the Track3P stage is selected.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

2\. Set the Modules Tab to Track3P/Multipacting

In the :guilabel:`Attribute Editor` panel, select the :guilabel:`Modules` tab, and in that tab set the :guilabel:`Analysis` to ``Track3P``, and then set the :guilabel:`Track3P` input to ``Multipacting``.

.. image:: images/window-modules2a.png
    :align: center

|

Below that is a field labeled :guilabel:`NERSC Data Directory`. This is for specifying the EM data that :program:`Track3P` will use to compute particle trajectories. For this example, we are going to set the :guilabel:`NERSC Data Directory` field to the path on the NERSC file system where the S3P results were computed in the first project stage. The steps to do this are outlined below; in brief they are to find the path to the S3P results from the first project stage, copy it to the system clipboard, and paste it into the Track3P :guilabel:`NERSC Data Directory` field.

.. rst-class:: step-number

3\. Get the Results Data Path

.. note::
  You must be logged into NERSC in order to complete this step.

a. First, make sure the :guilabel:`NERSC File Browser` panel is visible. If it is not currently displayed, locate and click its checkbox in the :guilabel:`View` menu.

b. In the :guilabel:`Analysis Stages` panel, select the :program:`S3P` stage you created previously.

c. In the :guilabel:`Simulation Jobs` panel, select the job you ran previously, so that you see the job details at the bottom of the panel.

d. Near the bottom of the :guilabel:`Simulation Jobs` panel, inline with the :guilabel:`Remote Directory` field, find the right-facing green arrow tool button and click it. (If this button is disabled, you will need to first login into NERSC to enable it.)

.. image:: images/window-open-nersc-browser-annotated.png
    :align: center

.. .. image:: images/window-copy-nersc-path.png
    :align: center

|

.. rst-class:: step-number

4\. Copy the Results Data Path

Clicking the tool button in the previous step will update the :guilabel:`NERSC File Browser` to display the remote directory. In the browser, locate the ``s3p_results`` item and right-click over it. A context menu will appear with two items. Select the first one, which is labeled :guilabel:`Copy Path to Clipboard`.

.. image:: images/window-s3pResults-copy.png
    :align: center

|

.. rst-class:: step-number

5\. Paste the Results Data Path

Now we can return to the Track3P stage and set the data directory field.

a. In the :guilabel:`Analysis Stages` panel, select the :program:`Track3P` stage.
b. In the :guilabel:`Attribute Editor` panel, find the :guilabel:`NERSC Data Directory` field and paste the path you copied in the previous step. You can either right-click on the field and select :guilabel:`Paste` or use :kbd:`Control-c` (:kbd:`Command-c` on macOS).

.. image:: images/window-modules2.png
    :align: center

|
