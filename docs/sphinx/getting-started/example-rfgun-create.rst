Create RfGun Project
====================

First, you should create your project.  For more detail on this step, you can refer back to the project creation page for the :ref:`Omega3P example<Creating an ACE3P Project>`.

For this example, you will be using the ``RfGunVacuum.gen`` file from the :program:`ACE3P` code workshops. You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632>`_ if needed.

The specific analyses you will be running are ``Omega3P`` (for the first stage), and ``TEM3P`` (for the second stage).

.. image:: images/rfgun-create-project.png
    :align: center

|
