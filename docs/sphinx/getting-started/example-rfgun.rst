Example: Multiple Stages (RfGun)
================================

As with the ``Window`` example, you will go through the steps to create a multi-stage :program:`ACE3P` project.  The addition of multiple stages to the project framework allows for more than one Analysis to be defined within a single project.

During this tutorial you will create a new project, specify the analysis parameters for your first stage, submit the analysis job to NERSC, and then view the results.  After that, you will add a second stage to our project and run through these same steps again for the new stage.  Before beginning this tutorial, it will be helpful to have first gone through the :ref:`Window tutorial<Example: Multiple Stages (Window)>`.  Certain steps will be only vaguely described, but are described in more detail in earlier tutorials.


.. toctree::
    :maxdepth: 1

    example-rfgun-create.rst
    example-rfgun-specify1.rst
    example-rfgun-addstage.rst
    example-rfgun-specify2.rst

.. note::
    Screenshots in this section were taken from Modelbuilder running on Windows 11.
