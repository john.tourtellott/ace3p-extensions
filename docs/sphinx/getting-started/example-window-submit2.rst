Run Track3P Analysis
====================

You are now ready to submit the Track3P analysis job. The steps are the same as described in :ref:`Running Omega3P on NERSC` and :ref:`Tracking Job Status`. Make sure you are logged into NERSC, then open the :guilabel:`ACE3P` menu and select the :guilabel:`Submit Analysis Job...` item to open the job-submit dialog. Enter your :guilabel:`Project repository`, then scroll down to the :guilabel:`Number of cores` field and enter ``32``. You can use the default values for the rest of the fields in the dialog.

.. important::
    Given the size of this simulation, be sure to change the :guilabel:`Number of cores` from ``1`` to ``32`` so that the job will run in a reasonable time.

.. image:: ./images/example-submit2-annotated.png
  :align: center
  :width: 67%

Once you submit the job, :program:`ModelBuilder for ACE3P` will add add a row for it in the :guilabel:`Simulation Jobs` panel and begin polling NERSC for status updates.
