Example: Remote Visualization
=============================
Because :program:`modelbuilder` is built on the :program:`ParaView` framework, it can use ParaView's remote visualization capabilities for interactive display and data exploration of large datasets stored in supercomputing resources. :program:`ParaView` is a client-server application. The :program:`ParaView` client (or simply paraview) will run on your desktop while the server will run at the remote supercomputing site. Running :program:`ParaView` remotely in a client-server configuration involves establishing an ssh tunnel to the login node, launching the :program:`ParaView` server, and connecting the server to the client over the tunnel via a socket. Because this can be a difficult task for new users and occasional users, :program:`ModelBuilder for ACE3P` streamlines and simplifies many of the steps needed for visualizing ACE3P datasets stored at NERSC.

To demonstrate the process, we will use the :program:`Omega3P` dataset generated in :ref:`Example: Omega3P Analysis`. In that previous example, you downloaded the results data from NERSC to you local machine. In this example, you will display the results data by running a ParaView server instance to render the files stored at NERSC.

.. important::
  SSH/MFA Acess is required for remote visualization.

  Before starting, users should set up their local computer to connect to NERSC using a :program:`Secure Shell` (SSH) client and Multi-Factor Authentication (MFA). Instructions for this can be found in the NERSC documention at https://docs.nersc.gov/connect/mfa/.

.. toctree::
    :maxdepth: 1

    remote-viz/configure.rst
    remote-viz/connect.rst
    remote-viz/disconnect.rst
    remote-viz/notes.rst

.. note::
  Screenshots in this section were taken from modelbuilder running on an Ubuntu system.

.. only:: draft

  The following describes the steps you will take to install ParaView on your desktop and configure it so that you can launch parallel jobs on remote systems from within the ParaView GUI.

  using distributed memory and computing resources on large scale machines.

  remote rendering capability to display data

  In this example, we will detail the steps requisite to perform an :program:`RF-postprocessing` analysis of a previous :program:`Omega3P` simulation. This example builds on the previous example for creating a project and running an :program:`Omega3P` simulation. Please ensure you have completed that example before moving on. Note that this procedure also applies to the postprocessing of :program:`T3P` and :program:`S3P` analyses.
